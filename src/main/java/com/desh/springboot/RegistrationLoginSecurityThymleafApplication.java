package com.desh.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RegistrationLoginSecurityThymleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegistrationLoginSecurityThymleafApplication.class, args);
	}

}
