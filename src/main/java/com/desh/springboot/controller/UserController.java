package com.desh.springboot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.desh.springboot.dto.UserRegistrationDto;
import com.desh.springboot.service.UserService;

@Controller
public class UserController {

	private UserService userService;

	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}

	@ModelAttribute("user")
	public UserRegistrationDto userRegistrationDto() {
		return new UserRegistrationDto();
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public String create() {
		return "users/registration";
	}

	@RequestMapping(value = "/store", method = RequestMethod.POST)
	public String store(@ModelAttribute("user") UserRegistrationDto userRegistrationDto) {
		userService.save(userRegistrationDto);
		return "redirect:/registration?success";

	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "users/login";
	}
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "users/index";
	}
}
