package com.desh.springboot.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.desh.springboot.dto.UserRegistrationDto;
import com.desh.springboot.model.User;

public interface UserService extends UserDetailsService {

	User save(UserRegistrationDto userRegistrationDto);

}
